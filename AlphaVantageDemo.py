import StockGraphCreator as sgc
import matplotlib.pyplot as plt
import CompanyStocks as stocks


def main():
    companyStock = stocks.CompanyStocks("GPRO", 'daily', 1)

    for s in companyStock.stock_list:
        print("Date: {} .. Price: {} .. Volume: {} .. Percent Trend: {}%"
              .format(s.date, s.price, s.volume, round(s.percent_trend,2)))


    for i in companyStock.stock_mountains:
        print(i.date)

    print("num of highs: {}".format(len(companyStock.stock_highs)))
    print("num of lows: {}".format(len(companyStock.stock_lows)))

    trend_lines = companyStock.get_peak_trend_lines()


    sgc.create_stock_graph(companyStock.stock_list, companyStock.symbol)
    sgc.plot_mountains(companyStock.stock_mountains)
    sgc.plot_peak_trend_lines(trend_lines)
    plt.show()





if __name__ == "__main__":
    main()



