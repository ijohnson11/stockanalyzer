import os
import json
import requests
from operator import attrgetter
from datetime import datetime
import Stock


class CompanyStocks:

    def __init__(self, symbol, funct, time_period):
        self.symbol = symbol
        #List of Stocks
        self.stock_list = []
        # List of Stocks that are at a peak between two other stocks
        self.stock_highs = []
        # List of Stocks that are at a valley between two other stocks
        self.stock_lows = []
        # List of stocks that are mountainous, very high spike in price
        self.stock_mountains = []
        # List of stocks that are valleys, very low dropoff in price
        self.stock_valleys =[]

        # Create a url string based on the information given to the class
        url = self.retrieve_stock_information(symbol, time_period, funct)


    def retrieve_stock_information(self, symbol, time_period, funct):
        websiteName = "https://www.alphavantage.co/query"
        function = funct
        symbol = symbol.upper()
        apiKey = "FVXCZJDSOFW87ODW"
        outputSize = "full"
        time_in_years = time_period

        if (function == "weekly" or function == "TIME_SERIES_MONTHLY_ADJUSTED"):
            url = "{}?function=TIME_SERIES_WEEKLY_ADJUSTED&symbol={}&apikey={}".format(websiteName, symbol, apiKey)
        elif(function == 'monthly'):
            url = "{}?function=TIME_SERIES_MONTHLY_ADJUSTED&symbol={}&apikey={}".format(websiteName, symbol, apiKey)
        elif (function == "daily"):
            url = "{}?function=TIME_SERIES_DAILY_ADJUSTED&symbol={}&outputsize={}&apikey={}".format(websiteName, symbol, outputSize, apiKey)

        self.create_stock_list(url, function, time_period)
        self.create_stock_highs()
        self.create_stock_lows()
        self.create_mountains()

    def create_stock_list(self, url, funct, time_period):
        # store the list of results in a dict
        json_results = requests.get(url).json()

        stock_dicts = {}
        if (funct == "daily"):
            stock_dicts = json_results.get('Time Series (Daily)')
        elif (funct == "weekly"):
            stock_dicts = json_results.get('Weekly Adjusted Time Series')
        elif (funct == "monthly"):
            stock_dicts = json_results.get("Monthly Adjusted Time Series")

        # Create a list of Stock objects containing stock date, price, and volume
        for x in stock_dicts:
            stock_date = x
            stock_price = float(stock_dicts[stock_date]['5. adjusted close'])
            stock_volume = stock_dicts[stock_date]['6. volume']
            stock_date = datetime.strptime(x, '%Y-%m-%d')
            # If the stock date is within the time period specified, add it to a list of stocks
            if ((datetime.now() - stock_date).days <= time_period * 365):
                #Create a new stock and append it a list
                stock = Stock.Stock(stock_date, stock_price, stock_volume)
                self.stock_list.append(stock)

        self.stock_list.reverse()

        for s in self.stock_list:
            s.calculate_percent_trend(self.stock_list)

    def create_stock_highs(self):
        for s in range(0, len(self.stock_list)):
            #If stock is first in list, it cannot be a high
            if s == 0:
                continue

            prev_stock = self.stock_list[s - 1]
            curr_stock = self.stock_list[s]

            #if stock is last in list and it is higher than the
            #previous stock, it is a high
            if s == len(self.stock_list) - 1:
                if curr_stock.price > prev_stock.price:
                    self.stock_highs.append(curr_stock)
            else:
                next_stock = self.stock_list[s+1]
                # If the current stock is higher than the previous and next, it is a peak
                if curr_stock.price > prev_stock.price and curr_stock.price > next_stock.price:
                    self.stock_highs.append(curr_stock)

    def create_stock_lows(self):
        for s in range(0, len(self.stock_list)):
            # If stock is first in list, it cannot be a high
            if s == 0:
                continue

            prev_stock = self.stock_list[s - 1]
            curr_stock = self.stock_list[s]

            # if stock is last in list and it is higher than the
            # previous stock, it is a high
            if s == len(self.stock_list) - 1:
                if curr_stock.price < prev_stock.price:
                    self.stock_lows.append(curr_stock)
            else:
                next_stock = self.stock_list[s + 1]
                # If the current stock is higher than the previous and next, it is a peak
                if curr_stock.price < prev_stock.price and curr_stock.price < next_stock.price:
                    self.stock_lows.append(curr_stock)

    def create_mountains(self):
        for i in range (0, len(self.stock_lows)):
            starting_low = self.stock_lows[i]

            for j in range(i+1, len(self.stock_lows)):
                potential_peaks = []
                next_low = self.stock_lows[j]
                high_point = 0

                highs = self.get_highs_between_lows(starting_low, next_low)
                for high in highs:
                    if high.price >= 1.05*starting_low.price:
                            potential_peaks.append(high)

                if potential_peaks:
                    high_point = max(potential_peaks, key = attrgetter('price'))
                    if next_low.price <= .95*high_point.price:
                        try:
                            self.stock_mountains.index(high_point)
                        except:
                             self.stock_mountains.append(high_point)
                             i = self.stock_lows.index(next_low)
                             break
                elif(not potential_peaks):
                    if starting_low.price > next_low.price:
                        break

    def get_highs_between_lows(self, starting_low, end_low):
        start_date = starting_low.date
        end_date = end_low.date
        highs_list = []
        for high in self.stock_highs:
            #If the high is between the two dates, add it to a list
            if(start_date < high.date < end_date):
                highs_list.append(high)
            #If the high date is greater than the end date, break the loop
            elif(high.date > end_date):
                break

        return highs_list

    def get_peak_trend_lines(self):
        stock_trend_lines = []

        i = 0
        while i < len(self.stock_mountains ) - 1:
            trend_line = []
            trend_line.append(self.stock_mountains[i])
            if self.stock_mountains[i].price < self.stock_mountains[i+1].price:
                increasing = True
            else:
                increasing = False

            if increasing:
                while increasing and i < len(self.stock_mountains ) - 1:
                    if self.stock_mountains[i].price < self.stock_mountains[i + 1].price:
                        trend_line.append(self.stock_mountains[i+1])
                        i+=1
                    else:
                        increasing = False
            else:
                while increasing == False and i < len(self.stock_mountains ) - 1:
                    if self.stock_mountains[i].price > self.stock_mountains[i + 1].price:
                        trend_line.append(self.stock_mountains[i+1])
                        i+=1
                    else:
                        increasing = True

            stock_trend_lines.append(trend_line)

        return stock_trend_lines