class Stock:

    def __init__(self, date, price, volume):
        self.date = date
        self.price = price
        self.volume = volume
        self.percent_trend = 0
        self.similar_stocks = []

    def calculate_percent_trend(self, stock_list):
        # Find index of stock in stock_list
        index = stock_list.index(self)

        if(index == 0):
            self.percent_trend = 0
        else:
            prev_stock = stock_list[index- 1]
            self.percent_trend = 100 *(1 - (prev_stock.price/self.price))


class TrendLine:
    def __init__(self, left_stock, right_stock):
        self.left_stock = left_stock
        self.right_stock = right_stock
        self.trend = 0