import os
import json
import requests
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.dates as dates
import numpy as np
from datetime import datetime,timedelta
from requests.auth import HTTPDigestAuth
import scipy.interpolate as interpolate
import StockPatternFinder as spf
import time

def plot_smooth_lines(x,y, linestyle, l):
    sorted_x = sorted(x)
    xnew = dates.date2num(sorted_x)
    f = interpolate.interp1d(xnew, y, kind='quadratic')
    x2 = np.linspace(min(xnew), max(xnew))

    plt.plot(x2, f(x2), linestyle , label= l)


def create_stock_graph(stock_list, sym):
    print('Creating graph for {}'.format(sym))
    plt.figure(sym)

   # Split the stock dates and prices for graphing
    stock_dates = []
    stock_prices =[]
    for stock in stock_list:
        stock_dates.append(stock.date)
        stock_prices.append(stock.price)


    #Get difference in highest and lowest values for incrementing y-axis
    highest_close = max(stock_prices)
    lowest_close = min(stock_prices)
    high_low_diff = highest_close - lowest_close

    plt.xlabel('Date')
    plt.ylabel('Stock Price(US $)')
    plt.title('Stock Chart for {}'.format(sym))
    plt.xticks(rotation = 45)
    plt.yticks(np.arange(lowest_close,highest_close, step=(high_low_diff/20)))
    plt.plot(stock_dates, stock_prices, 'k', label="Stock Prices")
    print('done')

def plot_mountains(stock_mountains):
    stock_dates = []
    stock_prices = []
    for s in stock_mountains:
        stock_dates.append(s.date)
        stock_prices.append(s.price)

    plt.plot(stock_dates, stock_prices, 'o', label="Stock Peak")

def plot_peak_trend_lines(trend_lines):
    for i in trend_lines:
        trend_line_dates = []
        trend_line_prices = []
        for j in i:
            trend_line_dates.append(j.date)
            trend_line_prices.append(j.price)
        plt.plot(np.unique(trend_line_dates),
                 np.poly1d(np.polyfit(dates.date2num(trend_line_dates), trend_line_prices, 1))(np.unique(dates.date2num(trend_line_dates))))
